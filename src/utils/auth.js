
const isLogged = () => !!localStorage.getItem('userName')
const login = user => localStorage.setItem('userName', user)
const logout = () => localStorage.removeItem('userName')

export { isLogged, login, logout }