const { default: axios } = require("axios");

export class StateService{
  async get() {
    const res = await axios.get('https://servicodados.ibge.gov.br/api/v1/localidades/estados');
    return res.data;
  }

}