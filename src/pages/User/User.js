import { Button, Input, InputNumber, Popconfirm, Select, Table } from 'antd';
import Modal from 'antd/lib/modal/Modal';
import React, { useEffect, useState } from 'react';
import { StateService } from '../../services/state';

const User = () => {
	const [isModalVisible, setIsModalVisible] = useState(false);
	const [states, setStates] = useState([]);
	const [users, setUsers] = useState([
		{
			key: new Date().getTime(),
			name: 'Matheus',
			age: 20,
			state: 'Piauí',
		},
	]);

	const [name, setName] = useState('');
	const [age, setAge] = useState(0);
	const [state, setStateBrasil] = useState('');
	const [key, setKey] = useState(0);

	const getUser = async () => {
		const res = await new StateService().get();
		setStates(res);
	};
	useEffect(() => {
		getUser();
	}, []);

	const handleOk = () => {
		if (!key) {
			setUsers([...users, { name, age, state, key: new Date().getTime() }]);
		} else {
			const indexUser = users.findIndex((user) => user.key === key);
			const usersAux = [...users];
			usersAux[indexUser] = { name, age, state, key };
			setUsers([...usersAux]);
		}
		setIsModalVisible(false);
	};

	const handleCancel = () => {
		setIsModalVisible(false);
	};
	const handleDelete = (row) => {
		const indexUser = users.findIndex((user) => user === row);
		const usersAux = [...users];
		usersAux.splice(indexUser, 1);
		setUsers(usersAux);
	};

	const showModal = () => {
		setIsModalVisible(!isModalVisible);
	};

	const columns = [
		{
			title: 'Nome',
			dataIndex: 'name',
			key: 'name',
		},
		{
			title: 'Idade',
			dataIndex: 'age',
			key: 'age',
		},
		{
			title: 'Estado',
			dataIndex: 'state',
			key: 'state',
		},
		{
			dataIndex: 'deletar',
			render: (e, record) =>
				users.length >= 1 ? (
					<Popconfirm
						onClick={(event) => {
							event.stopPropagation();
						}}
						title="Deseja deletar?"
						onConfirm={(event) => {
							event.stopPropagation();
							handleDelete(record);
						}}
						onCancel={(event) => {
							event.stopPropagation();
						}}
					>
						<a>Delete</a>
					</Popconfirm>
				) : null,
		},
	];

	const selectRow = ({ name, age, state, key }) => {
		setName(name);
		setAge(age);
		setStateBrasil(state);
		setKey(key);
		showModal();
	};
	const newRow = () => {
		setName('');
		setAge(0);
		setStateBrasil('');
		setKey(0);
		showModal();
	};

	return (
		<>
			<Modal
				title="Salvar Usuário"
				visible={isModalVisible}
				onOk={handleOk}
				onCancel={handleCancel}
			>
				<Input
					placeholder="Nome"
					value={name}
					className="mt-2"
					onChange={(event) => {
						setName(event.target.value);
					}}
				/>

				<InputNumber
					style={{ width: '100%' }}
					className="mt-2"
					placeholder="Idade"
					value={age}
					onChange={(age) => {
						setAge(age);
					}}
				/>

				<Select
					placeholder="Select a person"
					optionFilterProp="children"
					value={state}
					className="mt-2 full-width"
					onChange={(stateSelected) => {
						setStateBrasil(stateSelected);
					}}
					filterOption={(input, option) =>
						option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
					}
				>
					{states.map((stateSelected, index) => (
						<Select.Option value={stateSelected.nome} key={index}>
							{stateSelected.nome}
						</Select.Option>
					))}
				</Select>
			</Modal>
			<div className="row ant-row-end ">
				<Button type="primary" onClick={newRow}>
					Criar
				</Button>
			</div>
			<Table
				dataSource={users}
				columns={columns}
				onRow={(record) => {
					return {
						onClick: () => {
							selectRow(record);
						},
					};
				}}
			/>
		</>
	);
};

export default User;
