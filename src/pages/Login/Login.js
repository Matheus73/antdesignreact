import { Button, Card, Input, Form } from 'antd';
import logo from '../../assets/img/logo-virtex.png';
import React, { useState } from 'react';
import './Login.css';
import { login as sigIn } from '../../utils/auth';
import { history } from '../../utils/history';

const Login = () => {
	const [userName, userNameValue] = useState('');
	const [password, passwordValue] = useState('');

	const onChangeUsername = (event) => {
		userNameValue(event.target.value);
	};

	const onChangePassword = (event) => {
		passwordValue(event.target.value);
	};

	const login = () => {
		sigIn(userName);
		history.push('/')
	};

	return (
		<div className="login">
			<Card className="card-login">
				<Form onFinish={() => login()}  name="basic" initialValues={{ remember: true }}>
					<Form.Item>
						<div className="row">
							<img src={logo} alt="Logo da Virtex" />
						</div>
					</Form.Item>
					<Form.Item
						name="username"
						rules={[{ required: true, message: 'Adicione seu Usuário' }]}
					>
						<Input
							placeholder="Usuário"
							value={userName}
							onChange={onChangeUsername}
						/>
					</Form.Item>
					<Form.Item
						name="password"
						rules={[{ required: true, message: 'Adicione sua senha' }]}
					>
						<Input.Password
							placeholder="Senha"
							value={password}
							onChange={onChangePassword}
						/>
					</Form.Item>
					<div className="row justify-center">
						<Button type="primary" htmlType="submit" danger>
							Entrar
						</Button>
					</div>
				</Form>
			</Card>
		</div>
	);
};

export default Login;
