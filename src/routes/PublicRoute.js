import React from "react";
import { Redirect, Route } from "react-router";
import { isLogged } from "../utils/auth";

const PublicRoute = (props) =>
  !isLogged() ? <Route {...props} /> : <Redirect to="/" />;

export default PublicRoute;