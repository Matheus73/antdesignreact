import React from 'react';
import { Redirect, Route } from 'react-router';
import { isLogged } from '../utils/auth';
import LayoutPrivate from '../layout/LayoutPrivate/LayoutPrivate';


const PrivateRoute = (props) =>
	isLogged() ? (
		<LayoutPrivate>
			<Route {...props} />
		</LayoutPrivate>
	) : (
		<Redirect from="*" to="/login" />
	);

export default PrivateRoute;
