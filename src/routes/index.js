import React, {  } from 'react';
import { Route, Router, Switch } from 'react-router';
import PrivateRoute from './PrivateRoute';
import PublicRoute from './PublicRoute';
import { history } from '../utils/history';
import { NotFound } from '../pages/NotFound';
import Login from '../pages/Login/Login';
import Home from '../pages/Home/Home';
import User from '../pages/User/User';

const Routes = () => {
		return (
			<Router history={history}>
				<Switch>
					<PublicRoute exact path="/login" component={Login} />
					<PrivateRoute exact path="/" component={Home} />
					<PrivateRoute exact path="/user" component={User} />
					<Route component={NotFound} />
				</Switch>
			</Router>
		);
	
}

export default Routes;