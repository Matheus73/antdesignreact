import { Layout, Menu } from 'antd';

import './LayoutPrivate.css';
import logo from '../../assets/img/logo-virtex.png';

import {
	MenuUnfoldOutlined,
	MenuFoldOutlined,
	UserOutlined,
  HomeOutlined,
} from '@ant-design/icons';
import React, { useState } from 'react';
import { history } from '../../utils/history';

const { Header, Sider, Content } = Layout;

const MainLayout = (props) => {
	const [collapsed, collapsedValue] = useState(false);

	const toggle = (event) => {
		collapsedValue(event);
	};

	return (
		<Layout>
			<Sider trigger={null} collapsible collapsed={collapsed}>
				<div className="logo" />
				<Menu theme="dark" mode="inline" defaultSelectedKeys={['1']}>
					<img src={!collapsed ? logo : 'https://virtex.com.br/og-image.jpg'} alt="Logo" className="logo" />
          
					<Menu.Item key="1" icon={<HomeOutlined />}  onClick={() =>{history.push('/')}}>
						Home
					</Menu.Item>
					<Menu.Item key="2" icon={<UserOutlined />} onClick={() =>{history.push('/user')}}>
						User
					</Menu.Item>
				</Menu>
			</Sider>
			<Layout className="site-layout">
				<Header className="site-layout-background" style={{ padding: 0 }}>
					{React.createElement(
						collapsed ? MenuUnfoldOutlined : MenuFoldOutlined,
						{
							className: 'trigger',
							onClick: () => {
								toggle(!collapsed);
							},
						},
					)}
				</Header>
				<Content
					className="site-layout-background"
					style={{
						margin: '24px 16px',
						padding: 24,
						minHeight: 280,
					}}
				>
					{props.children}
				</Content>
			</Layout>
		</Layout>
	);
};
export default MainLayout;
